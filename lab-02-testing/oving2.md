# INF112 Øvelse 2, Våren 2022

# Andre øvelse
Hvis du eller gruppen din har gått glipp av noe i [første øvelse](../lab-01-intro/oving1.md), ta en titt på det først. Vi begynner med å teste TextAligner litt mer, så dere bør helst ha gjort den delen.

## Del -1: Flere ressurser

* [NerdSchoolBergen har en fin oversikt](https://github.com/nerdschoolbergen/all-about-testing-code) over testing, med flere øvelser. Dette er et praktisk rettet kurs som ble holdt for UiB-studenter for noen år siden, og øvelsene starter med ganske enkle ting. Fint sted å begynne om man blir stående fast på «gjett hvordan du tester dette».

## Del 0: Planlegging

Dere skal skrive en del tester (og kanskje annen kode) – tenk rask gjennom hvordan dere vil gjøre det. Sitte sammen i par hvor en skriver og en tenker? Prøve hver for dere og så se på det sammen og forklare tankegangen? 

## Del 1: TextUtils

[TextUtils](https://git.app.uib.no/inf112/22v/textutils) prosjektet er nå oppdatert med en [del implementasjoner av TextAligner](https://git.app.uib.no/inf112/22v/textutils/-/tree/master/src/main/java/no/uib/ii/inf112/impl).

##### Commit og push
Pass på å committe og pushe det dere gjorde sist. Så lenge du fulgte instruksene og brukte din egen *fork*, kan du pushe tilbake til den så mye som du vil. Når du har gjort det, må du oppdatere / *merge* inn de nyeste forbedringene:

##### Oppsett
<details><summary><b>GUIDE</b>: Oppdatere fork fra upstream</summary>

*  Vi antar at du har klonet/lastet ned din egen fork på vanlig måte (Import Projects from Git, eller ```git clone```), fra git.app.uib.no/DITT_BRUKERNAVN/textutils.git

Kopien du jobber på vil da være satt opp med din egen fork som *origin*, og Git vil pushe og pulle derfra. Vi skal nå legge til en «remote» til, slik at du har to «remotes»: *origin*, som er din egen fork, og *upstream* som er den du forket fra.

* Om du bruker kommandolinjen: gjør ```git remote add upstream https://git.app.uib.no/inf112/22v/textutils.git```. 

* I Eclipse: høyreklikk på prosjektet ditt, velg *Team → Show in Repositories View*.

    * Du får opp en tab et sted med tittel "Git Repositories", hvor prosjektet ditt er valgt.

    * Høyreklikk på *Remotes*, velg *Create Remote*

    * Velg navn (f.eks. "upstream"), og *Configure pull*, trykk *OK*.

    * I neste dialog, fyll inn ```https://git.app.uib.no/inf112/22v/textutils.git```

   * Velg *Save*.

##### Oppdateringer
For å få nye oppdateringer fra Anya, gjør

* På kommandolinjen:
   * ```git pull upstream master``` *eller*
   * ```git fetch upstream master``` etterfulgt av ```git merge upstream/master``` (eller ```git rebase upstream/master```)
* I Eclipse:
   * *Team → Pull...* og velg *upstream*.

(Du kan pulle fra Eclipse selv om du gjorde oppsettet fra kommandolinjen, og omvendt.)

#### Konflikt
Det kan godt være du får en konflikt hvis du har endret noen av de samme filene som er endret i upstream. Denne må i såfall håndteres før du kan fortsette utviklingen. Om nødvendig, se om noen kan hjelpe deg, se etter løsning på Stack Overflow, eller prøv en av disse guidene: http://weblog.masukomi.org/2008/07/12/handling-and-avoiding-conflicts-in-git/ http://wiki.eclipse.org/EGit/User_Guide#Resolving_a_merge_conflict

</details>

## Teste tester
Finn frem testene fra forrige øvelse, og prøv å kjøre dem på de [nye implementasjonene](https://git.app.uib.no/inf112/22v/textutils/-/tree/master/src/main/java/no/uib/ii/inf112/impl) av `TextAligner` – `AlignerA`–`AlignerH`. Du kan evt. bruke [denne klassen](https://git.app.uib.no/inf112/22v/textutils/-/blob/master/src/test/java/no/uib/ii/inf112/TestAligner.java) som utgangspunkt for å lage en test som sjekker alle implementasjonene fra `A`–`H`.

`AlignerA`–`H` inneholder alle forskjellige feil, med mulig unntak av `AlignerH` som kanskje er korrekt (bla. avhengig av hvilken oppførsel du forventer). Hvis det er implementasjoner (unntatt `H`) hvor *ingen* av testene feiler, så er ikke testene bra nok! Utvid med tester som avslører feilene – helst uten å se på `AlignerA`–`H` implementasjonene.

* Fant dere noen bugs dere ikke forventet? Eller som dere mener ikke egentlig er feil?
* Fant dere noen feil i `AlignerH`?
* Ville `E`, `F` eller `G` varianten vært ‘bra nok’?
 
Det går også an å ta en titt på [løsingen fra forrige uke](../lab-01-intro/losning1.md) (trykk på *SPOILERS*) for tips til hva som er / kan gå galt.

## Del 2: 🦆🐤🐤🐤🐤🐤

En viktig kunde har fått en alvorlig feil i [ande-simuleringssystemet sitt](https://git.app.uib.no/inf112/22v/textutils/-/tree/master/src/main/java/no/uib/ii/inf112/pond): Det dukker plutselig opp for mange andunger + at endene plutselig også begynner å gå fortere.

Simuleringen er satt opp slik:

* En Pond inneholder PondObjects – enten Duck eller Duckling
* Dette er en [stegvis simulering](https://en.wikipedia.org/wiki/Discrete-event_simulation) – for hvert steg i simuleringen kalles en `step` på alle objektene, som oppdaterer tilstanden deres
* For `Duck`, så er det meningen den skal flytte seg litt bortover, og så – for hvert 25. steg, «klekker» den en andunge, som fortsetter som selvstendig objekt, med samme retning og fart som moren.
* `Duckling` bare beveger seg foreløpig (vi har vært for opptatt med å finne den vanskelige feilen til å implementere at de vokser opp og lever selvstendige liv)

[`Pond`](https://git.app.uib.no/inf112/22v/textutils/-/blob/master/src/main/java/no/uib/ii/inf112/pond/Pond.java) inneholder en bitteliten main-metode som kan brukes for å se hva som skjer, f.eks:

```
                                                          …
                                                         🦆🐤                                                                     
                                                        🦆🐤🐤🐤                                                                    
                                                    🦆🐤🐤🐤                                                                        
                                                🦆🐤🐤🐤                                                                            
                                                …
```

Forventet resultat er ca. slik (med kjedelig repetisjon komprimert):
```
                                                       …
                                                        🦆
                                                       🦆
                                                      🦆
                                                      … 22 steg                                                                    
                                                    🦆🐤
                                                    … 24 steg                                                                    
                                                  🦆🐤🐤
                                                  … 24 steg                                                                        
                                                🦆🐤🐤🐤                                                                            
                                                …
```


Prosjektet har dessverre ingen tester (vi har vært for opptatt med å legge til features og finne emojis til å ha rukket å gjøre noe med testing og sånn…).

*Pga. forretningshemmeligheter og slikt vil kunden helst at dere, om mulig, unngår å se på klassene i `pond.impl` pakken.*

* Diskuter hvilke tester dere vil trenge for å finne ut hva som er galt – særlig hvis dere ikke kan se implementasjonen.
* Hvordan vil dere håndtere testing av en stegvis simulering? Særlig når en del av de interessante tingene skjer et stykke inn i simuleringen (etter 25 steg)? ([Husk Given-When-Then](https://martinfowler.com/bliki/GivenWhenThen.html))
* Det kan godt være det er smart å begynne med å teste de enkleste tingene før dere går løs på hva som skjer i `step`
* Hele `pond`-pakken mangler tester, men den ligge inne i samme prosjektet som `TextAligner`, så dere slipper å sette opp JUnit – bare kopier og ta utgangspunkt i en av testene som [ligger der fra før](https://git.app.uib.no/inf112/22v/textutils/-/tree/master/src/test/java/no/uib/ii/inf112).

**HINT:** *Bugs er sosiale dyr* – det er påfallende ofte at det er mer enn bare én feil som forårsaker problemet.

Del 3: Refleksjon

Tenk litt gjennom hvordan dere håndterte øvelsen: fant dere en god måte å jobbe på? hadde alle mulighet til å bidra? hva gjorde dere om dere sto fast på praktiske problemer og trengte hjelp? gikk det greit å håndtere at du selv eller noen andre gjorde feil eller ikke fant ut av ting? hva lærte dere om hva som er lurt å teste og hvordan det er lurt å teste?

